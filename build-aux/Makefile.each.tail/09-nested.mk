# Copyright (C) 2016-2018  Luke Shumaker
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

_nested.targets := $(notdir $(filter-out $(at.targets),$(addprefix $(outdir)/,$(nested.targets))))
$(eval $(foreach _tmp.nested,$(_nested.targets),\
  $$(outdir)/$(_tmp.nested): $$(addsuffix /$(_tmp.nested),$$(call at.addprefix,$$(outdir),$$(nested.subdirs)))$(at.nl)))
.PHONY: $(addprefix $(outdir)/,$(_nested.targets))

at.subdirs += $(nested.subdirs)
at.targets += $(addprefix $(outdir)/,$(_nested.targets))
