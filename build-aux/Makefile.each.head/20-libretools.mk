# Copyright (C) 2017-2018  Luke Shumaker
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

pkgconfdir    = $(sysconfdir)/libretools.d
pkglibexecdir = $(libexecdir)/libretools
pkglibdir     = $(libdir)/libretools

# Auto-detect ########################################################

libretools.pkg = libretools

libretools.src.devtools =

libretools.out.mans     = $(patsubst %.ronn,%,$(libretools.src.ronn))
libretools.out.bins     = $(libretools.src.exec)
libretools.out.libexecs =
libretools.out.libs     = $(libretools.src.sh)
libretools.out.docs     = $(libretools.src.md)
libretools.out.confs    = $(libretools.src.conf)

libretools.out  = $(libretools.out.mans)
libretools.out += $(libretools.out.bins)
libretools.out += $(libretools.out.libexecs)
libretools.out += $(libretools.out.libs)
libretools.out += $(libretools.out.docs)
libretools.out += $(libretools.out.confs)

libretools.pots  = $(libretools.out.bins)
libretools.pots += $(libretools.out.libexecs)
libretools.pots += $(libretools.out.libs)
