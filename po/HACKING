Translations for programs are provided in files with the `.po` suffix.
These are created by copying PO-template (`.pot`) files, and filling
in the missing "msgstr" values.

To add a translation, you'll first (1) need to create the `.pot` files
if you don't already have them, and then (2) copy them and (3) fill
them in.

# 1. Create `.pot` files

  If you are working from a release source tarball, you can skip this
  step (the `.pot` files are included in the tarball); if you are
  working from git, read on.

  libretools' .pot files are not tracked in git; they are created by
  running `make`.  You'll need a checkout of both "devtools-par" and
  "libretools" to run `make` if building from git.

      $ git clone https://git.parabola.nu/packages/devtools-par.git/
      $ git clone https://git.parabola.nu/packages/libretools.git/
      $ cd libretools
      $ make po/files.generate
      ...
      $ cd po/
      $ ls
      HACKING
      gitget.pot
      librelib.pot
      libretools.pot
      ...

# 2. Create `.po` files from them

  Create a folder under `po/` with the two-letter language code[^1]
  you want to add.  Then, copy the `.pot` files to that folder,
  changing the file extension to `.po`.  Finally, link the `subdir.mk`
  file to be the Makefile for that directory.  For example:

      $ mkdir es
      $ for file in *.pot; do cp $file es/${file%t}; done
      $ ln -s ../subdir.mk es/Makefile

  [^1]: See the langauge code table
        here: <http://www.lingoes.net/en/translator/langcode.htm>

# 3. Fill the `.po` files in with translations

  From there you can open each .po file in your favorite text editor,
  and fill in the `msgstr` values with translations of the associated
  `msgid` keys.
