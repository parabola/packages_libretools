include $(dir $(lastword $(MAKEFILE_LIST)))/../../config.mk
include $(topsrcdir)/build-aux/Makefile.head.mk

locale = $(notdir $(srcdir))
domains = $(patsubst %.po,%,$(filter %.po,$(files.src.src)))

$(outdir)/%.mo: $(srcdir)/%.po $(call at.path,$(srcdir)/../%.pot)
	msgcmp --use-untranslated --use-fuzzy $< $(word 2,$^)
	msgfmt -c --use-fuzzy -o $@ $<

$(DESTDIR)$(localedir)/$(locale)/LC_MESSAGES/%.mo: $(outdir)/%.mo
	install -T -Dm644 '$<' '$@'

define _domain_rule
files.out.$(domain) += $(domain).mo
files.sys.$(domain) += $(localedir)/$(locale)/LC_MESSAGES/$(domain).mo
endef
$(eval $(foreach domain,$(domains),$(_domain_rule)$(at.nl)))

at.subdirs += ..

include $(topsrcdir)/build-aux/Makefile.tail.mk
