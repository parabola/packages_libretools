include $(dir $(lastword $(MAKEFILE_LIST)))/config.mk
include $(topsrcdir)/build-aux/Makefile.head.mk

libretools.out.mans =
libretools.out.bins =
libretools.out.libexecs =
libretools.out.libs =
libretools.out.docs =
libretools.out.confs =

files.src.gen += .srcversion-libretools.mk .srcversion-devtools.mk
nested.subdirs = src po

$(outdir)/check:
	cd $(@D)/test && ./testenv $(TESTENVFLAGS) bats cases

$(outdir)/shellcheck: private shellcheck.flags = --exclude=1090,1091,2016,2059,2064,2164,2191
$(outdir)/shellcheck: private shellcheck.prune = -false
$(outdir)/shellcheck: private shellcheck.prune += -o -type d -name doc
$(outdir)/shellcheck: private shellcheck.prune += -o -type d -name man
$(outdir)/shellcheck: private shellcheck.prune += -o -type f -name indent
$(outdir)/shellcheck: private shellcheck.prune += -o -type f -name Makefile
$(outdir)/shellcheck: private shellcheck.prune += -o -type f -name makepkg.gen
$(outdir)/shellcheck: private shellcheck.prune += -o -type f -name source.sh.gen
$(outdir)/shellcheck:
	cd $(@D)/test && ./testenv $(TESTENVFLAGS) 'cd "$$TMPDIR/destdir" && find \( $(shellcheck.prune) \) -prune -o -not -type d -exec shellcheck $(shellcheck.flags) {} +'

include $(topsrcdir)/build-aux/Makefile.tail.mk
