#!/usr/bin/env bash
. libremessages

flag "-a $(_ OPTARG)" 'Flag 1' \
     '-b'             'Flag 2' \
     "-c $(_ OPTARG)" 'Flag 3' \
     "-d $(_ OPTARG)" 'Flag 4' \
     "-e $(_ OPTARG)" 'Flag 5' \
     "-f $(_ OPTARG)" 'Flag 6' \
     "-g $(_ OPTARG)" 'Flag 7' \
     "-h $(_ OPTARG)" 'Flag 8'

flag "-A $(_ OPTARG)" 'FLAG 1' \
     '-B'             'FLAG 2'

flag '-1' 'FLAG A' \
     '-2' 'FLAG B'
