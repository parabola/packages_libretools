#!/usr/bin/env bash
. libremessages

flag 'Settings:' \
     "-C"               "Force create mode (don't download)" \
     "-D"               "Force download mode (don't create)" \
     "-p <$(_ FILE)>"   "Use an alternate build script (instead of
                         'PKGBUILD').  If an SRCBUILD exists in the same
                         directory, it is used instead" \
     'Alternate modes:' \
     "-g, --geninteg"   "Generate integrity checks for source files" \
     "-S, --srcbuild"   "Print the effective build script (SRCBUILD)" \
     "-M, --makepkg"    "Generate and print the location of the
                         effective makepkg script" \
     "-h, --help"       "Show this message"
