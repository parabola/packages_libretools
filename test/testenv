#!/usr/bin/env bash
{
	set -e

	# Parse the arguments
	NETWORK=true
	SUDO=true
	while [[ $# -gt 0 ]]; do
		case "$1" in
			--no-network) shift; unset NETWORK;;
			--network) shift; NETWORK=true;;
			--no-sudo) shift; unset SUDO;;
			--sudo) shift; SUDO=true;;
			--) shift; break;;
			*) break;;
		esac
	done
	export NETWORK SUDO

	if [[ $# == 0 ]]; then
		echo 'You need to run testenv with arguments!' >&2
		exit 1
	fi

	# Set up the working directory, and add the hook to clean it up
	export TMPDIR="$(mktemp --tmpdir -d libretools-test.XXXXXXXXXX)"
	cleanup() {
		set +e
		# coordinate this with ./lib/common.bash
		if [[ $SUDO ]] && [[ -d "$TMPDIR/chroots" ]]; then
			if [[ "$(stat -f -c %T "$TMPDIR/chroots")" == btrfs ]]; then
				sudo find "$TMPDIR/chroots" -depth -inum 256 -exec \
				     btrfs subvolume delete {} \; &>/dev/null
			fi
			sudo rm -rf -- "$TMPDIR/chroots"
		fi
		rm -rf -- "$TMPDIR"
	}
	trap cleanup EXIT

	# Set up the install to work with
	destdir=$TMPDIR/destdir

	old_pwd="$(pwd)"
	if [[ -f $0 ]]; then
		cd "${0%/*}"
	fi
	if ! make -C .. install prefix=/usr sysconfdir=/etc DESTDIR="$destdir" &>"$TMPDIR/make-output"; then
		echo 'error creating local install, cannot run tests' >&2
		cat "$TMPDIR/make-output"
		exit 1
	fi
	cd "$old_pwd"

	# Set up the environment
	export PATH="$destdir/usr/bin:$PATH"
	export LIBRETOOLS_LIBDIR="$destdir/usr/lib/libretools"
	export _librelib_conf_sh_sysconfdir="$destdir/etc"
	export _librelib_conf_sh_pkgconfdir="$destdir/etc/libretools.d"
	sed -i 's,/usr/bin/librefetch,$(which librefetch),' \
	    "${_librelib_conf_sh_sysconfdir}/makepkg.d/librefetch.conf"

	# Hack to respect our variables in sudo
	install -Dm755 /dev/stdin "$destdir/usr/bin/testsudo" <<-'eot'
		#!/bin/bash
		vars=(
			TMPDIR
			GNUPGHOME XDG_CACHE_HOME XDG_CONFIG_HOME
			PATH LIBRETOOLS_LIBDIR _librelib_conf_sh_sysconfdir _librelib_conf_sh_pkgconfdir
			GPGKEY
		)
		env=()
		for var in "${vars[@]}"; do
			env+=("$var=${!var}")
		done
		sudo "${env[@]}" "$@"
		eot
	# Hack to work around GnuPG being stupid with locating gpg-agent's socket
	install -Dm755 /dev/stdin "$destdir/usr/bin/gpg" <<-'eot'
		#!/bin/bash
		export GNUPGHOME="${GNUPGHOME:-$HOME/.gnupg}"
		if [[ $GNUPGHOME = "$HOME/.gnupg" ]]; then
			export HOME=/var/empty
		fi
		exec /usr/bin/gpg "$@"
		eot
	install -Dm755 /dev/stdin "$destdir/usr/bin/gpg-connect-agent" <<-'eot'
		#!/bin/bash
		export GNUPGHOME="${GNUPGHOME:-$HOME/.gnupg}"
		if [[ $GNUPGHOME = "$HOME/.gnupg" ]]; then
			export HOME=/var/empty
		fi
		exec /usr/bin/gpg-connect-agent "$@"
		eot
	# Hack to work around ssh ignoring HOME and instead looking the homedir in NSS
	install -Dm755 /dev/stdin "$destdir/usr/bin/ssh" <<-'eot'
		#!/bin/bash
		vars=(
			TMPDIR
			_HOME GNUPGHOME XDG_CACHE_HOME XDG_CONFIG_HOME
			_PATH LIBRETOOLS_LIBDIR _librelib_conf_sh_sysconfdir _librelib_conf_sh_pkgconfdir
			GPGKEY
		)
		export _HOME="$HOME"
		export _PATH="$PATH"
		exec /usr/bin/ssh \
		     -o SendEnv="${vars[*]}" \
		     -o IdentityFile="$HOME/.ssh/id_rsa" \
		     -o UserKnownHostsFile="$HOME/.ssh/known_hosts" \
		     -F "$HOME/.ssh/config" \
		     "$@"
		eot

	# Run the tests
	command -- "$@"
}
