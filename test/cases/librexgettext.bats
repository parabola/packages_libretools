load ../lib/common

@test "librexgettext displays help" {
	LC_ALL=C librexgettext -h >$tmpdir/stdout 2>$tmpdir/stderr

	[[ "$(sed 1q $tmpdir/stdout)" =~ Usage:.* ]]
	empty $tmpdir/stderr
}

@test "librexgettext fails with 0 args" {
	librexgettext >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	[[ $status != 0 ]]
	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}

passcase() {
	librexgettext fixtures/librexgettext/$BATS_TEST_NAME.sh > $tmpdir/actual.pot 2>$tmpdir/stderr
	empty $tmpdir/stderr
	diff -u fixtures/librexgettext/$BATS_TEST_NAME.pot $tmpdir/actual.pot
}

@test "librexgettext handles multiple skipped flags" { passcase; }
@test "librexgettext handles zero flags" { passcase; }
@test "librexgettext handles librefetch flags" { passcase; }

@test "librexgettext fails on missing final flag description" {
	librexgettext fixtures/librexgettext/$BATS_TEST_NAME.sh > /dev/null 2>$tmpdir/stderr || status=$?

	[[ $status != 0 ]]
	[[ "$(sed 1q $tmpdir/stderr)" = "fixtures/librexgettext/$BATS_TEST_NAME.sh:4:"* ]]
}


@test "librexgettext fails on subshell flag descriptions" {
	librexgettext fixtures/librexgettext/$BATS_TEST_NAME.sh > /dev/null 2>$tmpdir/stderr || status=$?

	[[ $status != 0 ]]
	[[ "$(sed 1q $tmpdir/stderr)" = "fixtures/librexgettext/$BATS_TEST_NAME.sh:4-6:"* ]]
}

@test "librexgettext doesnt keep failing" {
	librexgettext some_file_that_doesnt_exist >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	[[ $status != 0 ]]
	empty $tmpdir/stdout
	[[ "$(wc -l <$tmpdir/stderr)" == 1 ]]
}

@test "librexgettext handles multiple files" {
	librexgettext fixtures/librexgettext/combine1.sh fixtures/librexgettext/combine2.sh > $tmpdir/actual.pot 2>$tmpdir/stderr
	empty $tmpdir/stderr
	diff -u fixtures/librexgettext/combine.pot $tmpdir/actual.pot
}
