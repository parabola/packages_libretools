load ../lib/common

@test "librechroot creates repo for new chroots" {
	require network sudo || skip
	testsudo librechroot -l "$BATS_TEST_NAME" run test -r /repo/repo.db
}

@test "librechroot cleans the local repo correctly" {
	require network sudo || skip
	testsudo librechroot -l "$BATS_TEST_NAME" make
	testsudo librechroot -l "$BATS_TEST_NAME" clean-repo
	testsudo librechroot -l "$BATS_TEST_NAME" run test -r /repo/repo.db
	# TODO: inspect /repo/* more
}

@test "librechroot respects exit status if out isnt a tty" {
	require network sudo || skip
	set -o pipefail
	{ testsudo librechroot -l "$BATS_TEST_NAME" run bash -c 'exit 3' | cat; } || status=$?

	[[ $status == 3 ]]
}

@test "librechroot creates ca certificates" {
	require network sudo || skip
	testsudo librechroot -l "$BATS_TEST_NAME" run test -r /etc/ssl/certs/ca-certificates.crt
}

@test "librechroot disables networking when requested" {
	require network sudo || skip

	    testsudo librechroot -l "$BATS_TEST_NAME"    run curl https://repo.parabola.nu/ >/dev/null
	not testsudo librechroot -l "$BATS_TEST_NAME" -N run curl https://repo.parabola.nu/ >/dev/null
}

@test "librechroot handles CHROOTEXTRAPKG correctly" {
	require network sudo || skip

	not testsudo librechroot -l "$BATS_TEST_NAME" run lsof
	echo "CHROOTEXTRAPKG=(lsof)" >> "$XDG_CONFIG_HOME"/libretools/chroot.conf
	testsudo librechroot -l "$BATS_TEST_NAME" install-name lsof
	testsudo librechroot -l "$BATS_TEST_NAME" clean-pkgs
	testsudo librechroot -l "$BATS_TEST_NAME" run lsof
	echo "CHROOTEXTRAPKG=()" >> "$XDG_CONFIG_HOME"/libretools/chroot.conf
	testsudo librechroot -l "$BATS_TEST_NAME" clean-pkgs
	not testsudo librechroot -l "$BATS_TEST_NAME" run lsof
}

@test "librechroot obeys depends in PKGBUILD" {
	require network sudo || skip

	# Make sure it's not in the base install
	not testsudo librechroot -l "$BATS_TEST_NAME" run lsof
	# Make sure it removes it without the PKGBUILD
	testsudo librechroot -l "$BATS_TEST_NAME" install-name lsof
	testsudo librechroot -l "$BATS_TEST_NAME" run lsof
	testsudo librechroot -l "$BATS_TEST_NAME" clean-pkgs
	not testsudo librechroot -l "$BATS_TEST_NAME" run lsof
	# Make sure it leaves it with the PKGBUILD
	testsudo librechroot -l "$BATS_TEST_NAME" install-name lsof
	testsudo librechroot -l "$BATS_TEST_NAME" run sh -c 'printf "%s\n" "$1" > /startdir/PKGBUILD' -- "$(cat fixtures/librechroot/PKGBUILD-arches)"
	# uid=99 is 'nobody', but we say '99' because sometimes 'nobody' is uid=65534.
	# https://bugs.archlinux.org/task/56828
	testsudo librechroot -l "$BATS_TEST_NAME" run chown -R 99:99 /startdir
	testsudo librechroot -l "$BATS_TEST_NAME" clean-pkgs
	testsudo librechroot -l "$BATS_TEST_NAME" run lsof
}

@test "librechroot can install libretools with chcleanup" {
	require network sudo || skip

	not testsudo librechroot -l "$BATS_TEST_NAME" run test -f /usr/bin/chcleanup
	testsudo librechroot -l "$BATS_TEST_NAME" run mkdir /startdir
	testsudo librechroot -l "$BATS_TEST_NAME" run sh -c 'printf "%s\n" "$1" > /startdir/PKGBUILD' -- "$(cat fixtures/librechroot/PKGBUILD-libretools)"
	# uid=99 is 'nobody', but we say '99' because sometimes 'nobody' is uid=65534.
	# https://bugs.archlinux.org/task/56828
	testsudo librechroot -l "$BATS_TEST_NAME" run chown -R 99:99 /startdir
	testsudo librechroot -l "$BATS_TEST_NAME" clean-pkgs
	testsudo librechroot -l "$BATS_TEST_NAME" run test -f /usr/bin/chcleanup
}

@test "librechroot displays help as normal user" {
	rm -rf "$XDG_CONFIG_HOME"
	LC_ALL=C librechroot help >$tmpdir/stdout 2>$tmpdir/stderr

	[[ "$(sed 1q $tmpdir/stdout)" =~ Usage:.* ]]
	empty $tmpdir/stderr
}

@test "librechroot otherwise fails as normal user" {
	librechroot -l "$BATS_TEST_NAME" run true >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	[[ $status != 0 ]]
	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}

@test "librechroot displays help and fails with 0 args" {
	LC_ALL=C librechroot -l "$BATS_TEST_NAME" >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	[[ $status != 0 ]]
	empty $tmpdir/stdout
	[[ "$(sed -n 2p $tmpdir/stderr)" =~ Usage:.* ]]
}

@test "librechroot obeys the n flag" {
	require network sudo || skip

	not test -f "$chrootdir/$BATS_TEST_NAME/$BATS_TEST_NAME/$BATS_TEST_NAME.stamp"

	testsudo librechroot -n "$BATS_TEST_NAME" -l "$BATS_TEST_NAME" run touch /"$BATS_TEST_NAME.stamp"

	test -f "$chrootdir/$BATS_TEST_NAME/$BATS_TEST_NAME/$BATS_TEST_NAME.stamp"
}

# requires sudo so we know it's not failing because it needs root
@test "librechroot fails for unknown commands" {
	require sudo || skip
	testsudo librechroot phony >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	[[ $status != 0 ]]
	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}

# requires sudo so we know it's not failing because it needs root
@test "librechroot fails for unknown flags" {
	require sudo || skip
	testsudo librechroot -q >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	[[ $status != 0 ]]
	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}

@test "librechroot fails when syncing a copy with itself" {
	require sudo || skip
	testsudo timeout 5 librechroot -l root sync  || status=$?
	case $status in
		0|124|137) # success|timeout+TERM|timeout+KILL
			false;;
		*)
			true;;
	esac
}

@test "librechroot deletes copies" {
	require network sudo || skip
	testsudo librechroot -l "$BATS_TEST_NAME" make
	test -d "$chrootdir/default/$BATS_TEST_NAME"
	testsudo librechroot -l "$BATS_TEST_NAME" delete
	not test -e "$chrootdir/default/$BATS_TEST_NAME"
}

@test "librechroot deletes subvolumes recursively" {
	require network sudo btrfs || skip
	testsudo librechroot -l "$BATS_TEST_NAME" make
	testsudo librechroot -l "$BATS_TEST_NAME" install-name btrfs-progs
	test -d "$chrootdir/default/$BATS_TEST_NAME"
	not test -e "$chrootdir/default/$BATS_TEST_NAME/var/subvolume"
	testsudo librechroot -l "$BATS_TEST_NAME" run btrfs subvolume create /var/subvolume
	test -d "$chrootdir/default/$BATS_TEST_NAME/var/subvolume"
	testsudo librechroot -l "$BATS_TEST_NAME" delete
	not test -e "$chrootdir/default/$BATS_TEST_NAME"
}

@test "librechroot cleans up TMPDIR" {
	require network sudo || skip

	local dir="$tmpdir/tmp"
	mkdir -- "$dir"

	TMPDIR=$dir testsudo librechroot -l "$BATS_TEST_NAME" -A x86_64 make

	# Make sure $dir is now empty
	rmdir -- "$dir"
}
