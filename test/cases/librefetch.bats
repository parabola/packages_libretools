load ../lib/common

setup() {
	common_setup

	cat >> "$XDG_CONFIG_HOME/pacman/makepkg.conf" <<-eot
		DLAGENTS=('ftp::/usr/bin/curl -fC - --ftp-pasv --retry 3 --retry-delay 3 -o %o %u'
		          'http::/usr/bin/curl -fLC - --retry 3 --retry-delay 3 -o %o %u'
		          'https::/usr/bin/curl -fLC - --retry 3 --retry-delay 3 -o %o %u'
		          'rsync::/usr/bin/rsync --no-motd -z %u %o'
		          'scp::/usr/bin/scp -C %u %o')
		BUILDDIR=""
		source ${_librelib_conf_sh_sysconfdir@Q}/makepkg.d/librefetch.conf
		eot
	export MAKEPKG_CONF="$XDG_CONFIG_HOME/pacman/makepkg.conf"

	install -Dm644 /dev/stdin "$XDG_CONFIG_HOME/libretools/librefetch.conf" <<-eot
		MIRRORS=("phony://example.com/dir/")
		DOWNLOADER=/usr/bin/false
		eot
}

@test "librefetch displays help" {
	LC_ALL=C librefetch -h >$tmpdir/stdout 2>$tmpdir/stderr

	[[ "$(sed 1q $tmpdir/stdout)" =~ Usage:.* ]]
	empty $tmpdir/stderr
}

@test "librefetch fails with bad flags" {
	local srcball=testpkg-1.0.tar.gz
	cp fixtures/librefetch/* "$tmpdir/"
	cd "$tmpdir"
	mv PKGBUILD{-mksource,}

	librefetch --bogus-flag libre://"$srcball" >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	[[ $status != 0 ]]
	empty $tmpdir/stdout
	not empty $tmpdir/stderr
	not test -e $tmpdir/workdir/srcdest/$srcball
}

# This test used to be called "librefetch cleans src-libre first", but let's
# be honest: it checks pretty much everything related to normal
# operation.
@test "librefetch runs with mksource" {
	local srcball=testpkg-1.0.tar.gz
	cp fixtures/librefetch/* "$tmpdir/"
	cd "$tmpdir"
	mv PKGBUILD{-mksource,}

	# Create garbage, to verifiy that it cleans src-libre first
	mkdir -p src-libre/foo
	touch src-libre/foo/file

	# Run librefetch
	makepkg -g >& log.txt

	# Verify that no temporary files were left around
	not test -e librefetch.*

	# Verify that there were no warnings about missing backup=()
	# files
	not grep -F etc/testpkg.conf log.txt

	# Verify:
	# - The srcball was created...
	# - ... and is in the correct directory
	# - The srcball does not contain the garbage created earlier
	# - The files in the srcball are in the correct order (if the
	#   order isn't ensured, then this would only sometimes fail,
	#   unfortunately).
	bsdtar tf "$tmpdir/workdir/srcdest/$srcball" > list-pkg.txt
	diff -u list.txt list-pkg.txt
	# Verify that the signature was created and matches
	gpg --quiet --verify "$tmpdir/workdir/srcdest/$srcball"{.sig,} 2>/dev/null
}

@test "librefetch runs with srcbuild" {
	local srcball=testpkg-1.0.tar.gz
	cp fixtures/librefetch/* "$tmpdir/"
	cd "$tmpdir"
	mv PKGBUILD{-srcbuild,}
	mv SRCBUILD{-srcbuild,}

	# Create garbage, to verifiy that it cleans src-libre first
	mkdir -p src-libre/foo
	touch src-libre/foo/file

	# Run librefetch
	makepkg -g

	# Verify that no temporary files were left around
	not test -e librefetch.*

	# Verify:
	# - The srcball was created...
	# - ... and is in the correct directory
	# - The srcball does not contain the garbage created earlier
	# - The files in the srcball are in the correct order (if the
	#   order isn't ensured, then this would only sometimes fail,
	#   unfortunately).
	bsdtar tf "$tmpdir/workdir/srcdest/$srcball" > list-pkg.txt
	diff -u list.txt list-pkg.txt
	# Verify that the signature was created and matches
	gpg --quiet --verify "$tmpdir/workdir/srcdest/$srcball"{.sig,} 2>/dev/null
}

@test "librefetch recurses" {
	local srcball=testpkg-1.0.tar.gz
	cp fixtures/librefetch/* "$tmpdir/"
	cd "$tmpdir"
	mv PKGBUILD{-recurse,}

	makepkg -g
	bsdtar tf "$tmpdir/workdir/srcdest/$srcball" > list-pkg.txt
	diff -u list.txt list-pkg.txt
	gpg --quiet --verify "$tmpdir/workdir/srcdest/$srcball"{.sig,} 2>/dev/null
}

@test "librefetch doesnt recurse extra" {
	local srcball=testpkg-1.0.tar.gz
	cp fixtures/librefetch/* "$tmpdir/"
	cd "$tmpdir"
	mv PKGBUILD{-recurse,}

	:> "$tmpdir/workdir/srcdest/$srcball"
	makepkg -g
	empty "$tmpdir/workdir/srcdest/$srcball"
	gpg --quiet --verify "$tmpdir/workdir/srcdest/$srcball"{.sig,} 2>/dev/null
}
