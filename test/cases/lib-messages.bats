load ../lib/common

@test "libremessages can be included twice" {
	. libremessages
	. libremessages
}

@test "libremessages can be included with set euE" {
	( # use a subshell because BATS doesn't like `set -u`
		set -euE
		. libremessages
	)
}

@test "libremessages works with no color and set euE" {
	(
		unset TERM
		set -euE
		. libremessages
		msg Foo
	) >$tmpdir/stdout 2>$tmpdir/stderr

	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}

@test "libremessages can be called without including" {
	libremessages msg Foo >$tmpdir/stdout 2>$tmpdir/stderr

	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}

@test "libremessages fails with msg and no args" {
	libremessages msg || status=$?
	[[ $status != 0 ]]
}

@test "libremessages allows subheadings to flag" {
	# Note that old versions of `flag` panicked if given an odd
	# number of headings, so give an odd number here.
	libremessages flag \
		      -a adesc \
		      -b bdesc \
		      Head1: \
		      -c cdesc > $tmpdir/out
	cat > $tmpdir/exp <<-eot
		  -a            adesc
		  -b            bdesc
		 Head1:
		  -c            cdesc
		eot
	diff -u $tmpdir/exp $tmpdir/out
}

@test "libremessages is quiet on stdout on errs" {
	LC_ALL=C bash -euE -c '. libremessages; setup_traps; false' >"$tmpdir/stdout" 2>"$tmpdir/stderr" || status=$?

	[[ $status != 0 ]]
	empty "$tmpdir/stdout"
	grep '==> ERROR:' "$tmpdir/stderr"
}
