load ../lib/common

@test "librestage displays usage text" {
	rm -rf "$XDG_CONFIG_HOME"
	LC_ALL=C librestage -h >$tmpdir/stdout 2>$tmpdir/stderr

	[[ "$(sed 1q "$tmpdir/stdout")" =~ Usage:.* ]]
	empty "$tmpdir/stderr"
}

@test "librestage fails with 0 args" {
	librestage >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	[[ $status != 0 ]]
	empty "$tmpdir/stdout"
	not empty "$tmpdir/stderr"
}

@test "librestage fails with invalid args" {
	librestage -q >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	[[ $status != 0 ]]
	empty "$tmpdir/stdout"
	not empty "$tmpdir/stderr"
}

@test "librestage guesses the repo" {
	nochroot=false; require network sudo || nochroot=true
	mkdir -p -- "$tmpdir/reponame/libretools-hello"
	cp fixtures/librestage/PKGBUILD-hello "$tmpdir/reponame/libretools-hello/PKGBUILD"
	cd "$tmpdir/reponame/libretools-hello"

	if $nochroot; then
		makepkg
	else
		testsudo libremakepkg -l "$BATS_TEST_NAME"
	fi
	librestage

	find "$tmpdir" -not -type d -exec ls -ld -- {} +
	globfile $tmpdir/workdir/staging/reponame/libretools-hello-1.0-1-any.pkg.tar.?z
	$nochroot || globfile $tmpdir/workdir/staging/sources/parabola/libretools-hello-1.0-1-any.src.tar.?z
}

@test "librestage stages packages without PKGDEST" {
	nochroot=false; require network sudo || nochroot=true

	cat >> "$XDG_CONFIG_HOME/pacman/makepkg.conf" <<-eot
		PKGDEST=''
		SRCPKGDEST=''
		eot

	cp fixtures/librestage/PKGBUILD-hello "$tmpdir/PKGBUILD"
	cd "$tmpdir"

	if $nochroot; then
		makepkg
	else
		testsudo libremakepkg -l "$BATS_TEST_NAME"
	fi
	librestage repo1

	find "$tmpdir" -not -type d -exec ls -ld -- {} +
	globfile $tmpdir/workdir/staging/repo1/libretools-hello-1.0-1-any.pkg.tar.?z
	$nochroot || globfile $tmpdir/workdir/staging/sources/parabola/libretools-hello-1.0-1-any.src.tar.?z
}
