load ../lib/common

setup() {
	common_setup

	local ssh_port

	# Configure and start the SSH server
	install -Dm644 /dev/stdin "$tmpdir/etc/ssh/sshd_config" <<-eot
		AuthorizedKeysCommand /usr/bin/cat ${HOME}/.ssh/id_rsa.pub
		AuthorizedKeysCommandUser ${USER}
		PasswordAuthentication no
		AcceptEnv TMPDIR
		AcceptEnv _HOME GNUPGHOME XDG_CACHE_HOME XDG_CONFIG_HOME
		AcceptEnv _PATH LIBRETOOLS_LIBDIR _librelib_conf_sh_sysconfdir _librelib_conf_sh_pkgconfdir
		AcceptEnv GPGKEY
		ForceCommand HOME=\$_HOME; PATH=\$_PATH; eval "\$SSH_ORIGINAL_COMMAND"
		eot
	ssh-keygen -A -f "$tmpdir"
	ssh_port=$(./lib/runserver "$tmpdir/sshd.pid" \
		sshd -i \
		     -f "$tmpdir/etc/ssh/sshd_config" \
		     -h "$tmpdir/etc/ssh/ssh_host_ecdsa_key")

	# Configure the SSH client
	mkdir -p -- "$HOME/.ssh"
	ssh-keygen -N '' -f "$HOME/.ssh/id_rsa"
	ssh-keyscan -p "$ssh_port" 127.0.0.1 > "$HOME/.ssh/known_hosts"
	touch -- "$HOME/.ssh/config"

	# Configure libretools
	cat >> "$XDG_CONFIG_HOME/libretools/libretools.conf" <<-eot
		REPODEST=ssh://${USER@Q}@127.0.0.1:${ssh_port@Q}/${tmpdir@Q}/srv-staging/
		DBSCRIPTS_CONFIG=/etc/dbscripts/config.local.phony
		HOOKPRERELEASE=:
		HOOKPOSTRELEASE=:
		eot
}

teardown() {
	xargs -a "$tmpdir/sshd.pid" kill --

	common_teardown
}

@test "librerelease displays usage text" {
	rm -rf "$XDG_CONFIG_HOME"
	LC_ALL=C librerelease -h >"$tmpdir/stdout" 2>"$tmpdir/stderr"

	[[ "$(sed 1q "$tmpdir/stdout")" =~ Usage:.* ]]
	empty "$tmpdir/stderr"
}

@test "librerelease lists all files" {
	local workdir="$tmpdir/workdir"
	mkdir -p "$workdir/staging/repo1" "$workdir/staging/repo2/sub"
	touch \
		"$workdir/staging/repo1/file1" \
		"$workdir/staging/repo1/file2" \
		"$workdir/staging/repo2/file with spaces" \
		"$workdir/staging/repo2/sub/subfolder"

	LC_ALL=C librerelease -l &>"$tmpdir/list" || { status=$?; cat "$tmpdir/list"; return $status; }

	cat > "$tmpdir/list-correct" <<-eot
		  -> repo1
		     file1
		     file2
		  -> repo2
		     file with spaces
		     sub/subfolder
		eot

	diff "$tmpdir/list-correct" "$tmpdir/list"
}

@test "librerelease fails if gpgkey not set" {
	unset GPGKEY

	local workdir="$tmpdir/workdir"
	mkdir -p "$workdir/staging/repo1" "$workdir/staging/repo2/sub"
	touch \
		"$workdir/staging/repo1/file1" \
		"$workdir/staging/repo1/file2" \
		"$workdir/staging/repo2/file with spaces" \
		"$workdir/staging/repo2/sub/subfolder"

	LC_ALL=C librerelease -l >"$tmpdir/stdout" 2>"$tmpdir/stderr" || status=$?

	[[ $status != 0 ]]
	empty "$tmpdir/stdout"
	grep GPGKEY "$tmpdir/stderr"
}

@test "librerelease fails if DBSCRIPTS_CONFIG is not set" {
	cat >> "$XDG_CONFIG_HOME/libretools/libretools.conf" <<-eot
		DBSCRIPTS_CONFIG=''
		eot

	local workdir="$tmpdir/workdir"
	mkdir -p "$workdir/staging/repo1" "$workdir/staging/repo2/sub"
	touch \
		"$workdir/staging/repo1/file1" \
		"$workdir/staging/repo1/file2" \
		"$workdir/staging/repo2/file with spaces" \
		"$workdir/staging/repo2/sub/subfolder"

	LC_ALL=C librerelease -l >"$tmpdir/stdout" 2>"$tmpdir/stderr" || status=$?

	[[ $status != 0 ]]
	empty "$tmpdir/stdout"
	grep DBSCRIPTS_CONFIG "$tmpdir/stderr"
}

@test "librerelease runs" {
	# Add a stub db-update so that when we ssh to localhost it has
	# something to run.
	install -Dm755 /dev/stdin "$tmpdir/bin/db-update" <<-eot
		#!/bin/bash
		{
		    printf '%s\n' "\$DBSCRIPTS_CONFIG"
		    readlink -f -- "\$STAGING"
		    find "\$STAGING" -printf '%P\n' | LC_COLLATE=C sort
		} > ${tmpdir@Q}/log.txt
		eot
	PATH=$tmpdir/bin:$PATH

	# Log which directories the hooks are run in.
	cat >> "$XDG_CONFIG_HOME/libretools/libretools.conf" <<-eot
		HOOKPRERELEASE='pwd > ${tmpdir@Q}/prerelease.txt'
		HOOKPOSTRELEASE='pwd > ${tmpdir@Q}/postrelease.txt'
		eot

	# Make some files to stage
	local workdir="$tmpdir/workdir"
	mkdir -p "$workdir/staging/repo1" "$workdir/staging/repo2/sub"
	touch \
		"$workdir/staging/repo1/file1" \
		"$workdir/staging/repo1/file2" \
		"$workdir/staging/repo2/file with spaces" \
		"$workdir/staging/repo2/sub/subfolder"

	# Run
	librerelease

	# Make sure everything went OK
	pwd > "$tmpdir/pwd.txt"
	cat > "$tmpdir/log-correct.txt" <<-eot
		/etc/dbscripts/config.local.phony
		$(readlink -f -- "$tmpdir/srv-staging")

		repo1
		repo1/file1
		repo1/file1.sig
		repo1/file2
		repo1/file2.sig
		repo2
		repo2/file with spaces
		repo2/file with spaces.sig
		repo2/sub
		repo2/sub/subfolder
		repo2/sub/subfolder.sig
		eot
	diff -u "$tmpdir/log-correct.txt" "$tmpdir/log.txt"
	diff -u "$tmpdir/pwd.txt" "$tmpdir/prerelease.txt"
	diff -u "$tmpdir/pwd.txt" "$tmpdir/postrelease.txt"
}
