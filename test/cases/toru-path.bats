load ../lib/common

@test "toru-path displays help" {
	LC_ALL=C toru-path -h >$tmpdir/stdout 2>$tmpdir/stderr

	[[ "$(sed 1q $tmpdir/stdout)" =~ Usage:.* ]]
	empty $tmpdir/stderr
}

@test "toru-path runs" {
	cat >> "$XDG_CONFIG_HOME/libretools/libretools.conf" <<-eot
		TORUPATH=${tmpdir@Q}
		REPOS=(libre)
		eot
	cat >> "$HOME/.abs.conf" <<-eot
		ABSROOT=${PWD@Q}/fixtures/toru
		eot

	toru-path

	tcamgr list "$tmpdir/paths.tch"|LC_COLLATE=C sort|while read -r key; do
		val="$(tcamgr get "$tmpdir/paths.tch" "$key")"
		printf '«%q» -> «%q»\n' "$key" "$val"
	done > "$tmpdir/paths-actual.txt"

	printf '«%q» -> «%q»\n' \
	       BAR "$PWD/fixtures/toru/libre/split-base" \
	       BAZ "$PWD/fixtures/toru/libre/split-base" \
	       FOO "$PWD/fixtures/toru/libre/split-base" \
	       bar "$PWD/fixtures/toru/libre/split" \
	       baz "$PWD/fixtures/toru/libre/split" \
	       foo "$PWD/fixtures/toru/libre/split" \
	       simple "$PWD/fixtures/toru/libre/simple" \
	       split-base "$PWD/fixtures/toru/libre/split-base" \
	       > "$tmpdir/paths-expected.txt"

	diff "$tmpdir/paths-expected.txt" "$tmpdir/paths-actual.txt"
}

@test "toru-path sets torupath" {
	cat >> "$XDG_CONFIG_HOME/libretools/libretools.conf" <<-eot
		TORUPATH=${tmpdir@Q}
		REPOS=(libre)
		eot
	cat >> "$HOME/.abs.conf" <<-eot
		ABSROOT=${PWD@Q}/fixtures/toru
		eot

	mkdir -p "$tmpdir/x"
	T="$tmpdir/x" toru-path
	test -f "$tmpdir/x/paths.tch"
	not test -e "$tmpdir/paths.tch"
}
