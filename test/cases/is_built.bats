load ../lib/common

@test "is_built displays help" {
	LC_ALL=C is_built -h >$tmpdir/stdout 2>$tmpdir/stderr

	[[ "$(sed 1q $tmpdir/stdout)" =~ Usage:.* ]]
	empty $tmpdir/stderr
}

@test "is_built fails with 0 args" {
	is_built >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	[[ $status -gt 1 ]]
	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}

@test "is_built succeeds with 1 arg" {
	is_built sh >$tmpdir/stdout 2>$tmpdir/stderr

	empty $tmpdir/stdout
	empty $tmpdir/stderr
}

@test "is_built returns 1 for non existent package" {
	is_built phony-ne-package 100 >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	[[ $status == 1 ]]
	empty $tmpdir/stdout
	empty $tmpdir/stderr
}

@test "is_built returns 1 for future packages" {
	# If emacs ever goes rapid release, we might need to change this :P
	is_built emacs 100 >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	[[ $status == 1 ]]
	empty $tmpdir/stdout
	empty $tmpdir/stderr
}

@test "is_built returns 0 for past packages" {
	# If emacs ever goes rapid release, we might need to change this :P
	is_built emacs 1 >$tmpdir/stdout 2>$tmpdir/stderr

	empty $tmpdir/stdout
	empty $tmpdir/stderr
}
