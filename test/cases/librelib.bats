load ../lib/common

@test "librelib displays help and fails with 0 args" {
	librelib >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	empty $tmpdir/stdout
	[[ "$(sed 1q $tmpdir/stderr)" =~ Usage:.* ]]
	[[ $status != 0 ]]
}

@test "librelib fails with 2 args" {
	librelib a b >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	empty $tmpdir/stdout
	not empty $tmpdir/stderr
	[[ $status != 0 ]]
}

@test "librelib displays usage text" {
	librelib -h >$tmpdir/stdout 2>$tmpdir/stderr

	[[ "$(sed 1q $tmpdir/stdout)" =~ Usage:.* ]]
	empty $tmpdir/stderr
}

# Nothing in $(libdir) should be executable anymore (except that
# $(libexecdir)=$(libdir), and executable things go in there. But I
# digress, libremessages should not be executable anymore).
@test "librelib finds messages" {
	v1=$(librelib messages)
	v2=$(librelib libremessages)
	v3=$(librelib messages.sh)
	v4=$(librelib libremessages.sh)

	[[ -r "$v1" && ! -x "$v1" ]]
	[[ "$v1" == "$v2" ]]
	[[ "$v1" == "$v3" ]]
	[[ "$v1" == "$v4" ]]
}

# conf.sh is non-executable
@test "librelib finds conf" {
	v1=$(librelib conf)
	v2=$(librelib libreconf)
	v3=$(librelib conf.sh)
	v4=$(librelib libreconf.sh)

	[[ -r "$v1" && ! -x "$v1" ]]
	[[ "$v1" == "$v2" ]]
	[[ "$v1" == "$v3" ]]
	[[ "$v1" == "$v4" ]]
}

@test "librelib fails to find phony" {
	librelib phony >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	empty $tmpdir/stdout
	not empty $tmpdir/stderr
	[[ $status != 0 ]]
}
