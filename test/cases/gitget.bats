load ../lib/common

@test "gitget displays help" {
	LC_ALL=C gitget -h >$tmpdir/stdout 2>$tmpdir/stderr

	[[ "$(sed 1q $tmpdir/stdout)" =~ Usage:.* ]]
	empty $tmpdir/stderr
}

@test "gitget fails with 0 args" {
	gitget >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	[[ $status != 0 ]]
	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}

@test "gitget forces url for bare" {
	mkdir "$tmpdir/src"
	cd "$tmpdir/src"
	git init .
	git config --local user.email 'libretools-test@localhost'
	git config --local user.name 'Test Suite'
	echo a > a
	git add .
	git commit -m 'initial commit'
	cd ..
	gitget bare src dst.git
	cd dst.git
	[[ "$(git config --get remote.origin.url)" == "$tmpdir/src" ]]
	cd ..
	gitget bare "file://$PWD/src" dst.git || status=$?
	[[ $status != 0 ]]
	gitget -f bare "file://$PWD/src" dst.git
	cd dst.git
	[[ "$(git config --get remote.origin.url)" == "file://$tmpdir/src" ]]
}
