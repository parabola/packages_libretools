load ../lib/common

setup() {
	common_setup

	# Disable networking for blacklist.txt, and install a cached version instead
	cat >> "$XDG_CONFIG_HOME/libretools/libretools.conf" <<-eot
		BLACKLIST='phony://example.com'
		eot
	install -Dm644 /dev/stdin "$XDG_CACHE_HOME/libretools/blacklist.txt" <<-eot
		linux:linux-libre:nonfree blobs and firmwares
		skype
		eot
}

@test "pkgbuild-check-nonfree displays usage text" {
	# This test seems silly, but it makes sure that it is executable,
	# syntactically correct,  and loading libraries works.
	LC_ALL=C pkgbuild-check-nonfree -h >$tmpdir/stdout 2>$tmpdir/stderr
	status=$?

	[[ "$(sed 1q $tmpdir/stdout)" =~ Usage:.* ]]
	empty $tmpdir/stderr
	[[ $status == 0 ]]
}

@test "pkgbuild-check-nonfree succeeds for free depends" {
	pkgbuild-check-nonfree fixtures/pkgbuild-check-nonfree/PKGBUILD.free >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	empty $tmpdir/stdout
	not empty $tmpdir/stderr
	[[ $status == 0 ]]
}

@test "pkgbuild-check-nonfree succeeds for nonfree depend with replacement" {
	pkgbuild-check-nonfree fixtures/pkgbuild-check-nonfree/PKGBUILD.nonfree-replacement >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	empty $tmpdir/stdout
	not empty $tmpdir/stderr
	[[ $status == 0 ]]
}

@test "pkgbuild-check-nonfree fails for nonfree depend" {
	pkgbuild-check-nonfree fixtures/pkgbuild-check-nonfree/PKGBUILD.nonfree >$tmpdir/stdout 2>$tmpdir/stderr || status=$?
	[[ $status != 0 ]]
	empty $tmpdir/stdout
	not empty $tmpdir/stderr

	local pcn_stat=$status

	pkgbuild-summarize-nonfree $pcn_stat >$tmpdir/stdout 2>$tmpdir/stderr || status=$?
	[[ $status != 0 ]]
	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}

@test "pkgbuild-check-nonfree fails when there is no blacklist" {
	rm $XDG_CACHE_HOME/libretools/blacklist.txt

	pkgbuild-check-nonfree fixtures/pkgbuild-check-nonfree/PKGBUILD.free >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	empty $tmpdir/stdout
	not empty $tmpdir/stderr
	[[ $status != 0 ]] && [[ $status != 15 ]]
}
