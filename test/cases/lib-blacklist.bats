load ../lib/common

@test "libreblacklist works with just pkgname" {
	v="$(libreblacklist normalize <<<skype)"; [[ $v == 'skype::::' ]]
	v="$(libreblacklist get-pkg <<<skype)"; [[ $v == skype ]]
	libreblacklist get-rep <<<irreplaceable | equals $'\n'
	libreblacklist get-url <<<skype | equals $'\n'
	libreblacklist get-reason <<<skype | equals $'\n'
}

@test "libreblacklist works with everything set" {
	line='linux:conflict:parabola:id:[semifree] blobs and firmware'
	v="$(libreblacklist normalize <<<"$line")"; [[ $v == "$line" ]]
	v="$(libreblacklist get-pkg <<<"$line")"; [[ $v == 'linux' ]]
	v="$(libreblacklist get-rep <<<"$line")"; [[ $v == 'libre/linux-libre' ]]
	v="$(libreblacklist get-url <<<"$line")"; [[ $v == 'https://labs.parabola.nu/issues/id' ]]
	v="$(libreblacklist get-reason <<<"$line")"; [[ $v == '[semifree] blobs and firmware' ]]
}

@test "libreblacklist normalizes correctly" {
	libreblacklist normalize <<<'#comment' | equals ''
	v="$(libreblacklist normalize <<<pkg)"; [[ $v == 'pkg::::' ]]
	v="$(libreblacklist normalize <<<pkg:)"; [[ $v == 'pkg::::' ]]
	v="$(libreblacklist normalize <<<pkg::)"; [[ $v == 'pkg::::' ]]
	v="$(libreblacklist normalize <<<pkg:rep)"; [[ $v == 'pkg:rep:::' ]]
	v="$(libreblacklist normalize <<<pkg:rep:)"; [[ $v == 'pkg:rep:::' ]]
	v="$(libreblacklist normalize <<<pkg:rep:ref)"; [[ $v == 'pkg:rep:ref::' ]]
	v="$(libreblacklist normalize <<<pkg:rep:ref:)"; [[ $v == 'pkg:rep:ref::' ]]
	v="$(libreblacklist normalize <<<pkg:rep:ref:id)"; [[ $v == 'pkg:rep:ref:id:' ]]
	v="$(libreblacklist normalize <<<pkg:rep:ref:id:)"; [[ $v == 'pkg:rep:ref:id:' ]]
	v="$(libreblacklist normalize <<<pkg:rep:ref:id:reason)"; [[ $v == 'pkg:rep:ref:id:reason' ]]
}

@test "libreblacklist works with colons in reason" {
	line='package:conflict:parabola:id:my:reason'
	v="$(libreblacklist normalize <<<"$line")"; [[ $v == "$line" ]]
	v="$(libreblacklist get-pkg <<<"$line")"; [[ $v == 'package' ]]
	libreblacklist get-rep <<<"$line" | equals $'\n'
	v="$(libreblacklist get-url <<<"$line")"; [[ $v == 'https://labs.parabola.nu/issues/id' ]]
	v="$(libreblacklist get-reason <<<"$line")"; [[ $v == 'my:reason' ]]
}

@test "libreblacklist prints urls only for valid references" {
	libreblacklist get-url <<<package:::id: | equals $'\n'
	libreblacklist get-url <<<package::unknown:id: | equals $'\n'
}

@test "libreblacklist fails update with no blacklist or network" {
	cat >> "$XDG_CONFIG_HOME/libretools/libretools.conf" <<-eot
		BLACKLIST='phony://example.com'
		eot

	libreblacklist update >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	[[ $status != 0 ]]
	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}

@test "libreblacklist fails cat with no blacklist or network" {
	cat >> "$XDG_CONFIG_HOME/libretools/libretools.conf" <<-eot
		BLACKLIST='phony://example.com'
		eot

	libreblacklist cat >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	[[ $status != 0 ]]
	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}

@test "libreblacklist fails update when BLACKLIST is unset" {
	cat >> "$XDG_CONFIG_HOME/libretools/libretools.conf" <<-eot
		BLACKLIST=
		eot

	libreblacklist update >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	[[ $status != 0 ]]
	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}

@test "libreblacklist fails cat when syntax error in conf" {
	# there is a stray single quote in there
	cat >> "$XDG_CONFIG_HOME/libretools/libretools.conf" <<-eot
		BLACKLIST='https://git.parabola.nu/blacklist.git/plain/blacklist.txt
		eot

	libreblacklist cat >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	[[ $status != 0 ]]
	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}

@test "libreblacklist downloads the blacklist as needed" {
	require network || skip

	libreblacklist cat >$tmpdir/stdout 2>$tmpdir/stderr

	not empty $tmpdir/stdout
}

@test "libreblacklist downloads the blacklist repeatedly" {
	require network || skip

	libreblacklist update
	libreblacklist update
}

@test "libreblacklist displays help and fails with no args" {
	LC_ALL=C libreblacklist >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	[[ $status != 0 ]]
	empty $tmpdir/stdout
	[[ "$(sed 1q $tmpdir/stderr)" =~ 'Usage: libreblacklist ' ]]
}

@test "libreblacklist displays help when given h" {
	LC_ALL=C libreblacklist -h >$tmpdir/stdout 2>$tmpdir/stderr

	[[ "$(sed 1q $tmpdir/stdout)" =~ 'Usage: libreblacklist ' ]]
	empty $tmpdir/stderr
}

@test "libreblacklist displays help when given h cat" {
	LC_ALL=C libreblacklist -h cat >$tmpdir/stdout 2>$tmpdir/stderr

	[[ "$(sed 1q $tmpdir/stdout)" == 'Usage: libreblacklist cat' ]]
	empty $tmpdir/stderr
}
