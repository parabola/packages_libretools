load ../lib/common

setup() {
	common_setup

	install -Dm644 /dev/stdin "$XDG_CONFIG_HOME/libretools/libredbdiff.conf" <<-'eot'
		statedir="$PWD"
		mirror_prbl='https://repo.parabola.nu/$repo/os/$arch'
		mirror_arch='https://mirrors.kernel.org/archlinux/$repo/os/$arch'
		repos=(libre)
		eot
}

@test "libredbdiff displays help" {
	LC_ALL=C libredbdiff -h >$tmpdir/stdout 2>$tmpdir/stderr

	[[ "$(sed 1q $tmpdir/stdout)" =~ Usage:.* ]]
	empty $tmpdir/stderr
}

@test "libredbdiff handles packages with multiple provides explicit" {
	cd fixtures/libredbdiff/statedir

	libredbdiff -n libre >$tmpdir/stdout 2>$tmpdir/stderr

	empty $tmpdir/stderr
	diff -w ../expected-explicit.txt $tmpdir/stdout
}

@test "libredbdiff handles packages with multiple provides implicit" {
	cd fixtures/libredbdiff/statedir

	libredbdiff -n >$tmpdir/stdout 2>$tmpdir/stderr

	empty $tmpdir/stderr
	diff -w ../expected-implicit.txt $tmpdir/stdout
}

@test "libredbdiff fails on bad repo args" {
	cd fixtures/libredbdiff/statedir

	libredbdiff -n bogus >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	[[ $status != 0 ]]
	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}
