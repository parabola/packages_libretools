load ../lib/common

setup() {
	common_setup

	cat >> "$XDG_CONFIG_HOME/pacman/makepkg.conf" <<-eot
		unset PKGDEST SRCPKGDEST
		eot
}

teardown() {
	local file
	for file in "$tmpdir"/*.pid; do
		[[ -f $file ]] || continue
		xargs -a "$file" kill --
	done

	common_teardown
}

@test "libremakepkg builds a trivial package" {
	require network sudo || skip
	cp fixtures/libremakepkg/PKGBUILD-hello "$tmpdir/PKGBUILD"
	cd "$tmpdir"

	testsudo libremakepkg -l "$BATS_TEST_NAME"

	globfile libretools-hello-1.0-1-any.pkg.tar.?z
}

@test "libremakepkg disables networking during prepare" {
	require network sudo || skip
	cp fixtures/libremakepkg/PKGBUILD-netprepare "$tmpdir/PKGBUILD"
	cd "$tmpdir"

	not testsudo libremakepkg -l "$BATS_TEST_NAME"
	not globfile libretools-netprepare-1.0-1-any.pkg.tar.?z
	testsudo libremakepkg -l "$BATS_TEST_NAME" -N
	globfile libretools-netprepare-1.0-1-any.pkg.tar.?z
}

@test "libremakepkg disables networking during build" {
	require network sudo || skip
	cp fixtures/libremakepkg/PKGBUILD-netbuild "$tmpdir/PKGBUILD"
	cd "$tmpdir"

	not testsudo libremakepkg -l "$BATS_TEST_NAME"
	not globfile libretools-netbuild-1.0-1-any.pkg.tar.?z
	testsudo libremakepkg -l "$BATS_TEST_NAME" -N
	globfile libretools-netbuild-1.0-1-any.pkg.tar.?z
}

@test "libremakepkg disables networking during package" {
	require network sudo || skip
	cp fixtures/libremakepkg/PKGBUILD-netpackage "$tmpdir/PKGBUILD"
	cd "$tmpdir"

	not testsudo libremakepkg -l "$BATS_TEST_NAME"
	not globfile libretools-netpackage-1.0-1-any.pkg.tar.?z
	testsudo libremakepkg -l "$BATS_TEST_NAME" -N
	globfile libretools-netpackage-1.0-1-any.pkg.tar.?z
}

@test "libremakepkg cleans the chroot before building" {
	require network sudo || skip
	# 1. First, we build testpkg1
	# 2. Then, we build testpkg2, which depends on testpkg1
	#    Therefore, testpkg1 will be installed after testpkg2 is built, we
	#    check for that.
	# 3. Then, we build hello, which depends on neither, so testpkg1 should
	#    be removed.

	# Also, do funny things with the output of libremakepkg to get a helpful
	# fail case.

	mkdir -p "$tmpdir"/{1,2,3}
	cp fixtures/libremakepkg/PKGBUILD-testpkg1 "$tmpdir/1/PKGBUILD"
	cp fixtures/libremakepkg/PKGBUILD-testpkg2 "$tmpdir/2/PKGBUILD"
	cp fixtures/libremakepkg/PKGBUILD-hello    "$tmpdir/3/PKGBUILD"


	cd "$tmpdir/1"
	testsudo libremakepkg -l "$BATS_TEST_NAME" &> "$tmpdir/out" || { status=$?; tail "$tmpdir/out"|cat -v; return $status; }

	cd "$tmpdir/2"
	testsudo libremakepkg -l "$BATS_TEST_NAME" &> "$tmpdir/out" || { status=$?; tail "$tmpdir/out"|cat -v; return $status; }
	testsudo librechroot -l "$BATS_TEST_NAME" run libretools-testpkg1 'first time, pass'

	# This next line is actually a separate test, but it fits in well with this test, and chroot tests are slow.
	# @test "libremakepkg doesnt cache local packages" {
	not testsudo librechroot -l "$BATS_TEST_NAME" run test -e /var/cache/pacman/pkg/libretools-testpkg1-1.0-1-any.pkg.tar.?z

	cd "$tmpdir/3"
	testsudo libremakepkg -l "$BATS_TEST_NAME" &> "$tmpdir/out" || { status=$?; tail "$tmpdir/out"|cat -v; return $status; }
	not testsudo librechroot -l "$BATS_TEST_NAME" run libretools-testpkg1 'second time, fail'
}

@test "libremakepkg handles PKGDEST not existing" {
	require network sudo || skip
	cp fixtures/libremakepkg/PKGBUILD-hello "$tmpdir/PKGBUILD"
	cd "$tmpdir"

	testsudo env PKGDEST="$tmpdir/dest/pkgdest" libremakepkg -l "$BATS_TEST_NAME"

	globfile dest/pkgdest/libretools-hello-1.0-1-any.pkg.tar.?z
}

@test "libremakepkg displays help as normal user" {
	rm -rf "$XDG_CONFIG_HOME"
	LC_ALL=C libremakepkg -h >$tmpdir/stdout 2>$tmpdir/stderr

	[[ "$(sed 1q $tmpdir/stdout)" =~ Usage:.* ]]
	empty $tmpdir/stderr
}

@test "libremakepkg otherwise fails as normal user" {
	# I do this to give it a chance of passing
	cp fixtures/libremakepkg/PKGBUILD-hello "$tmpdir/PKGBUILD"
	cd "$tmpdir"

	libremakepkg >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	[[ $status != 0 ]]
	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}

@test "libremakepkg fails if a hook fails" {
	require network sudo || skip
	cp fixtures/libremakepkg/PKGBUILD-hello "$tmpdir/PKGBUILD"
	cd "$tmpdir"

	cat >> "$XDG_CONFIG_HOME/libretools/libretools.conf" <<-eot
		BLACKLIST='phony://example.com'
		eot

	testsudo libremakepkg -l "$BATS_TEST_NAME" >$tmpdir/stdout 2>$tmpdir/stderr || status=$?

	[[ $status != 0 ]]
	tail -n1 $tmpdir/stderr | grep -qF '==> ERROR: Failure(s) in check_pkgbuild: check_pkgbuild_nonfree'
}

@test "libremakepkg detects distcc files" {
	require network sudo || skip
	cp fixtures/libremakepkg/PKGBUILD-hello "$tmpdir/PKGBUILD"
	cd "$tmpdir"

	cat >> "$XDG_CONFIG_HOME/libretools/chroot.conf" <<-eot
		CHROOTEXTRAPKG+=(distcc-nozeroconf socat)
		eot
	testsudo librechroot -l "$BATS_TEST_NAME" install-name distcc-nozeroconf socat

	# first make sure that the engine works
	testsudo libremakepkg -l "$BATS_TEST_NAME"
	globfile libretools-hello-1.0-1-any.pkg.tar.?z
	rm -f -- libretools-hello-1.0-1-any.pkg.tar.?z
	# now throw a wrench in it
	testsudo librechroot -l "$BATS_TEST_NAME" run touch /bin/distcc-tool
	# and make sure that the engine broke
	testsudo libremakepkg -l "$BATS_TEST_NAME" || status=$?
	[[ $status != 0 ]]
	not globfile libretools-hello-1.0-1-any.pkg.tar.?z
}

@test "libremakepkg forwards distcc ports" {
	require network sudo || skip

	# The maximum AF_UNIX socket path is 108 bytes; so let's have
	# a chroot name that's guaranteed to be >110 characters, to
	# make sure we handle that.
	local chrootname=$BATS_TEST_NAME.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa

	local distcc_port
	distcc_port=$(./lib/runserver "$tmpdir/distcc.pid" \
		echo 'Hello, I am a distcc server')

	cat >> "$XDG_CONFIG_HOME/libretools/chroot.conf" <<-eot
		CHROOTEXTRAPKG+=(distcc-nozeroconf socat)
		eot
	cat >> "$XDG_CONFIG_HOME/pacman/makepkg.conf" <<-eot
		DISTCC_HOSTS=127.0.0.1:${distcc_port@Q}
		eot

	cp fixtures/libremakepkg/PKGBUILD-distcc "$tmpdir/PKGBUILD"
	cd "$tmpdir"
	testsudo librechroot -l "$chrootname" install-name distcc-nozeroconf socat
	testsudo libremakepkg -l "$chrootname"
	globfile libretools-distcc-1.0-1-any.pkg.tar.?z
}

@test "libremakepkg doesnt symlink outputs" {
	require network sudo || skip

	sed -i /^unset/d "$XDG_CONFIG_HOME/pacman/makepkg.conf"

	cp fixtures/libremakepkg/PKGBUILD-hello "$tmpdir/PKGBUILD"
	cd "$tmpdir"

	testsudo libremakepkg -l "$BATS_TEST_NAME"

	not stat libretools-hello-1.0-1-any.pkg.tar.?z
	not stat libretools-hello-1.0-1-any.src.tar.?z
	globfile "$tmpdir/workdir/pkgdest"/libretools-hello-1.0-1-any.pkg.tar.?z
	globfile "$tmpdir/workdir/srcpkgdest"/libretools-hello-1.0-1-any.src.tar.?z
}

@test "libremakepkg succeeds with good signatures" {
	require network sudo || skip

	cp fixtures/libremakepkg/PKGBUILD-signed "$tmpdir/PKGBUILD"
	cp fixtures/libremakepkg/hello.sh "$tmpdir/hello.sh"
	cd "$tmpdir"
	gpg --detach-sign --use-agent --no-armor hello.sh

	testsudo libremakepkg -l "$BATS_TEST_NAME"
}

@test "libremakepkg fails with bad signatures" {
	require network sudo || skip

	cp fixtures/libremakepkg/PKGBUILD-signed "$tmpdir/PKGBUILD"
	cp fixtures/libremakepkg/hello.sh "$tmpdir/hello.sh"
	cd "$tmpdir"
	gpg --detach-sign --use-agent --no-armor hello.sh
	echo 'echo pwned' >> hello.sh
	makepkg -g >> PKGBUILD

	not testsudo libremakepkg -l "$BATS_TEST_NAME"
}

@test "libremakepkg does not run pkgver" {
	require network sudo || skip

	cp fixtures/libremakepkg/PKGBUILD-pkgver "$tmpdir/PKGBUILD"
	pushd "$tmpdir"

	testsudo libremakepkg -l "$BATS_TEST_NAME"

	globfile libretools-pkgver-1-1-any.pkg.tar.?z
	not globfile libretools-pkgver-2-1-any.pkg.tar.?z
	popd
	diff -u fixtures/libremakepkg/PKGBUILD-pkgver "$tmpdir/PKGBUILD"
}

@test "libremakepkg can re-use source-packages" {
	require network sudo || skip

	cp fixtures/libremakepkg/PKGBUILD-hello "$tmpdir/PKGBUILD"
	cd "$tmpdir"

	# I'm tempted to use file checksums here, but that would break
	# when we gain support for reproducible builds.  So just use
	# timestamps.

	testsudo libremakepkg -l "$BATS_TEST_NAME"
	globfile libretools-hello-1.0-1-any.src.tar.?z
	globfile libretools-hello-1.0-1-any.pkg.tar.?z
	a_stime=$(stat -c %Y -- libretools-hello-1.0-1-any.src.tar.?z)
	a_ptime=$(stat -c %Y -- libretools-hello-1.0-1-any.pkg.tar.?z)

	testsudo libremakepkg -l "$BATS_TEST_NAME" -S libretools-hello-1.0-1-any.src.tar.?z
	globfile libretools-hello-1.0-1-any.src.tar.?z
	globfile libretools-hello-1.0-1-any.pkg.tar.?z
	b_stime=$(stat -c %Y -- libretools-hello-1.0-1-any.src.tar.?z)
	b_ptime=$(stat -c %Y -- libretools-hello-1.0-1-any.pkg.tar.?z)

	(( a_stime == b_stime ))
	(( a_ptime < b_ptime ))
	(( a_ptime > a_stime ))
	(( b_ptime > a_stime ))
}
