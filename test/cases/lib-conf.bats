load ../lib/common

@test "lib/conf.sh sets makepkg vars in custom file" {
	unset PKGDEST
	touch "$tmpdir/makepkg.conf"
	bash -c "$(printf '. $(librelib conf.sh); MAKEPKG_CONF=%q set_var makepkg PKGDEST /pkgdest' "$tmpdir/makepkg.conf")"
	.  "$tmpdir/makepkg.conf"
	[[ $PKGDEST == /pkgdest ]]
}

@test "lib/conf.sh figures out HOME when root" {
	require sudo || skip
	# This one is tricky, because it does the job too well, it will find
	# the actual HOME, instead of the test environment HOME.  Therefore, we
	# will just check that [[ $HOME != /root ]]
	cd "$tmpdir"
	echo '. $(librelib conf.sh); echo "$LIBREHOME"' > test.sh
	LIBREHOME=$(testsudo bash ./test.sh)
	[[ $LIBREHOME != /root ]]
}

@test "lib/conf.sh nests LIBREUSER" {
	require sudo || skip
	[[ $USER != root ]]
	cd "$tmpdir"
	echo '. $(librelib conf.sh); echo "$LIBREUSER"' > test.sh

	LIBREUSER=$(testsudo bash ./test.sh)
	[[ $LIBREUSER == "$USER" ]]

	LIBREUSER=$(testsudo testsudo -u "$USER" bash ./test.sh)
	[[ $LIBREUSER == "$USER" ]]
}

@test "lib/conf.sh respects custom HOME" {
	cd "$tmpdir"
	echo '. $(librelib conf.sh); echo "$LIBREHOME"' > test.sh

	export HOME=/foo
	LIBREHOME=$(bash ./test.sh)

	[[ $LIBREHOME == /foo ]]
}
