#!/hint/bash

if [[ -z $LIBRETOOLS_LIBDIR || -z $_librelib_conf_sh_sysconfdir || -z $_librelib_conf_sh_pkgconfdir ]]; then
	libremessages error 'Must be run with ./testenv'
	exit 1
fi

# per-test setup/teardown ############################################

common_setup() {
	tmpdir="$(mktemp -d --tmpdir test.XXXXXXXXXXXX)"
	chmod 755 "$tmpdir"

	status=0

	# Clear the list of makepkg variables
	unset PKGDEST SRCDEST SRCPKGDEST LOGDEST BUILDDIR PKGEXT SRCEXT GPGKEY PACKAGER CARCH

	# Set up a test HOME
	export HOME="$tmpdir/home"
	export GNUPGHOME="$HOME/.gnupg"
	export XDG_CACHE_HOME="$HOME/.cache"
	export XDG_CONFIG_HOME="$HOME/.config"

	# Create a GPGKEY
	mkdir -p -- "$GNUPGHOME"
	chmod 700 -- "$GNUPGHOME"
	gpg --quiet --no-tty --batch --gen-key <<-eot
		Key-Type: default
		Key-Usage: sign
		Name-Real: Bob Tester
		Name-Email: tester@localhost
		Expire-Date: 0
		%no-protection
		%commit
		eot
	export GPGKEY="$(gpg --quiet --list-secret-keys --with-colons | awk -F: '/^sec:/{print substr($5,9)}')"

	# Configure libretools
	export chrootdir="${chrootdir:-$TMPDIR/chroots}"

	install -Dm644 /dev/stdin "$XDG_CONFIG_HOME"/libretools/libretools.conf <<-eot
		WORKDIR=${tmpdir@Q}/workdir
		BLACKLIST=https://git.parabola.nu/blacklist.git/plain/blacklist.txt
		eot
	install -Dm644 /dev/stdin "$XDG_CONFIG_HOME"/libretools/chroot.conf <<-eot
		CHROOTDIR=${chrootdir@Q}
		CHROOT=default
		CHROOTEXTRAPKG=()
		eot
	install -Dm644 /dev/stdin "$XDG_CONFIG_HOME"/pacman/makepkg.conf <<-eot
		PKGDEST=${tmpdir@Q}/workdir/pkgdest
		SRCDEST=${tmpdir@Q}/workdir/srcdest
		SRCPKGDEST=${tmpdir@Q}/workdir/srcpkgdest
		LOGDEST=${tmpdir@Q}/workdir/logdest
		PACKAGER='Bob Tester <tester@localhost>'
		eot
	mkdir -p -- "$tmpdir"/workdir/{pkg,src,srcpkg,log}dest
}

common_teardown() {
	gpg-connect-agent KILLAGENT /bye || true
	if [[ -n ${tmpdir:-} ]]; then
		if [[ -f "$tmpdir/.used-sudo" ]]; then
			sudo rm -rf -- "$tmpdir"
		else
			rm -rf -- "$tmpdir"
		fi
	fi
	# Clean up non-root chroots.  On btrfs it wouldn't be terrible
	# to wait until the end because CoW means that they don't take
	# up much space, but on other filesystems, we don't want to
	# require such a large disk.
	if [[ -n ${chrootdir:-} ]]; then
		local dir
		for dir in "$chrootdir"/*/*/; do
			if [[ -d $dir && $dir != */root/ ]]; then
				if [[ "$(stat -f -c %T "$dir")" == btrfs ]]; then
					sudo find "$dir" -depth -inum 256 -exec \
					     btrfs subvolume delete {} \; &>/dev/null
				fi
				sudo rm -rf -- "$dir"
			fi
		done
	fi
}

setup() {
	common_setup
}

teardown() {
	common_teardown
}

# Utility functions for use in test definitions ######################

require() (
	set +x
	local missing=()
	if libremessages in_array "network" "$@" && ! [[ $NETWORK ]]; then
		missing+=('networking')
	fi
	if libremessages in_array "sudo" "$@" && ! [[ $SUDO ]]; then
		missing+=('sudo')
	fi
	if libremessages in_array "btrfs" "$@" && ! [[ "$(stat -f -c %T "$chrootdir" 2>/dev/null || true)" == 'btrfs' ]]; then
		missing+=('btrfs')
	fi
	if (( ${#missing[@]} )); then
		return 1
	fi
	if libremessages in_array "sudo" "$@"; then
		touch "$tmpdir/.used-sudo"
	fi
	return 0
)

empty() {
	diff -u /dev/null "$1"
}

# Just using '!' doesn't trip `set -e`
not() (
	set +x
	# we don't care about what is in the file on 'not empty'
	# checks, so redefine 'empty' to be a bit quieter.
	empty() {
		[[ $(stat -c %s "$1") -eq 0 ]]
	}
	! eval "$@"
)

# Plain command substitution would remove trailing whitespace, despite
# being significant when testing for newline-terminated lines.
equals() {
	local stdin
	IFS= read -rd '' stdin || :
	[[ $1 == "$stdin" ]]
}

globfile() {
	[[ -f $1 ]]
}
