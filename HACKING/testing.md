Testing
=======

Please write unit tests for new things.  Tests can be run with `make
check`, which just runs `./testenv roundup` in the `test/` directory.
Relatedly, you need the `roundup` (the `sh-roundup` package on
Parabola) tool to run the tests.  `./testenv` can be given
`--no-network` and/or `--no-sudo` to dissable tests that require those
things.  Make can be made to pass those things in by setting
`TESTENVFLAGS`.  If you don't dissable either, I *strongly* recommend
setting TMPDIR to somewhere on a btrfs partition before running the
tests; otherwise the chroot tests will take forever.  I mean, they
already take long on btrfs, but without it... _dang_.

I also recommend having the `haveged` daemon running.  That's good
general advice, but also: some of the tests make GPG keys, this
"should" take on the order of 1 second, but can take several minutes
if you don't have `haveged` running.
