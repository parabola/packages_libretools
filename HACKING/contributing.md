Contributing
============

I'd love to have your patches!  Code should be hackable; if you want
to modify something, but can't figure out how: 1) ping me for help, 2)
it probably means the code was too complicated in the first place.

Patches should be sent to <dev@lists.parabola.nu>; please put
"[PATCH]" and "libretools" in the subject line.  If you have commit
access, but want me to look over it first, feel free to create a new
branch in git, and I will notice it.  Try to avoid pushing to the
"master" branch unless it's a trivial change; it makes it easier to
review things; though I *will* look over every commit before I do a
release, so don't think you can sneak something in :)

Be sure to make sure to follow the licensing requirements in
`HACKING/licensing.md`

I'd love to discuss possible changes on IRC (I'm lukeshu), either on
irc.freenode.net#parabola or in personal messages.  My account may be
online even if I'm not; I will eventually see your message, I do a
search for mentions of "luke" on #parabola every time I get on.

Please write unit tests for new functionality.  Or old functionality.
Please write unit tests!  See `HACKING/testing.md` for details on
testing.
