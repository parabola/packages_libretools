We don't require copyright assignment or any fancy paperwork!  Just
make sure you specify the license and include a copyright statement
with your name and the current year.

New code should (please) be licensed GPLv2+.  I say v2 instead of v3
because some code from Arch is GPLv2 (no "or any later version"), and
having to worry about which programs can be combined is a huge pain.

Copyright statements should look like

	# Copyright (C) YEARS NAME <EMAIL>

for most code, for 3rd-party code that has been imported, indent it a
bit:

	#   Copyright (C) YEARS NAME <EMAIL>

Always put a line with `# License:` specifying the license of that
file.
