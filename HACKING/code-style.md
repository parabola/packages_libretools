The style guidelines aren't terribly strict.  As long as things are
consistent per-file, I'm pretty happy.

Style guidelines
================

Unless you have a good reason, use `[[ ... ]]` instead of `[ ... ]`;
they work similarly, but `[[ ... ]]` is sometimes more readable (fine,
rarely, but never less), and is harder to make mistakes with quoting,
because it is syntactic magic, as opposed to `[ ... ]` which is an
executable which just happens to be implemented as a builtin.

Use a litteral tab for indent.  When indenting line-wrapped text, such
as that for `prose`, do it like this:  (» indicates tab, · indicates
space)

	func() {
	»   prose "This is the first line. This paragraph is going to be
	»   ·······wrapped."
	}

The `; then` and `; do` should go on the same line as
`if`/`elif`/`for`/`while`.  Also, there is no space before the `;`.

Prefer the `for VAR in LIST` syntax over the `for ((init; cond; inc))`
syntax, when possible.  For example (heh, `for` example):

	local i
	for (( i = 1 ; i <= 10 ; i++ )); do

should be

	local i
	for i in {1..10}; do

Of course, if the upper bound is a variable, the C-like syntax is
the better option, as otherwise you would have to use `seq` (calling
an external), or `eval` (gross, easy to mess up).

Indent comments like you would code; don't leave them at the beginning
of the line.  Example:

	for item in "${list[@]}"; do
		if [[ $item == foo ]]; then
	# BAD
			foobar
		fi
		if [[ $item == bar ]]; then
			# GOOD
			barbaz
		fi
	done

Fauno, I'm sorry.  But I don't know how you can read your own code :P.

Some people argue in favor of the useless use of cat, because data
should flow from left to right.  However, the input redirection
doesn't have to go on the right side of a command:

	cat file | program  # useless use of cat
	program < file      # data flows right to left
	< file program      # just right
