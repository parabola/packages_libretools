#!/usr/bin/env bash
# -*- coding: utf-8 -*-

# Copyright (C) 2011-2012 Michał Masłowski <mtjm@mtjm.eu>
# Copyright (C) 2012 Daniel Molina (lluvia)
# Copyright (C) 2017 Luke Shumaker <lukeshu@parabola.nu>
#
# License: GNU GPLv3+
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

. "$(librelib messages)"
. "$(librelib conf)"

usage() {
	print 'Usage: %s repo [arch]' "${0##*/}"
	echo
	prose "This script outputs a diff of package names and versions
	       in repo between pacman's sync db and abslibre checkout."
}

main() {
	load_conf libretools.conf WORKDIR || exit

	while getopts 'h' arg; do
		case "$arg" in
			h) usage; exit $EXIT_SUCCESS ;;
			*) usage >&2; exit $EXIT_INVALIDARGUMENT ;;
		esac
	done
	if [[ $# -ne 1 ]] && [[ $# -ne 2 ]]; then
		usage >&2
		exit $EXIT_INVALIDARGUMENT
	fi

	# The repo to find missing packages in.
	local repo=$1
	# The arch to check in Arch repos, other will have all arches checked.
	local arch=${2:-mips64el}

	diff -U0 \
	     <(pacman_list_packages "$repo" "$arch" | sort) \
	     <(abslibre_list_packages "$repo" "$arch" | sort) \
		| sed -rn 's/^[+-][^+-].+$/&/p'
}

pacman_list_packages() {
	local repo=$1
	local arch=$2

	# A Python tuple of repos which don't have arch=any packages.
	local archrepos='("core", "extra", "community")'

	tar xOf "/var/lib/pacman/sync/$repo.db" | python -c '
import sys
arch = None
name = None
version = None
it = iter(sys.stdin)
try:
    while True:
        line = next(it)
        if line == "%ARCH%\n":
            arch = next(it)
            if arch == "'"$arch"'\n" or "'"$repo"'" not in '"$archrepos"':
                print("%s-%s" % (name.strip(), version.strip()))
        if line == "%NAME%\n":
            name = next(it)
        if line == "%VERSION%\n":
            version = next(it)
except StopIteration:
    pass
'
}

abslibre_list_packages() {
	local repo=$1
	local arch=$2

	local CARCH=$arch
	local f name
	for f in "${WORKDIR}/abslibre/$repo"/* ; do
		load_PKGBUILD "$f/PKGBUILD" || continue
		if in_array 'any' "${arch[@]}" || in_array "$CARCH" "${arch[@]}" ; then
			for name in "${pkgname[@]}" ; do
				if [[ -z "$epoch" ]] ; then
					echo "$name-$pkgver-$pkgrel"
				else
					echo "$name-$epoch:$pkgver-$pkgrel"
				fi
			done
		fi
	done
}

main "$@"
