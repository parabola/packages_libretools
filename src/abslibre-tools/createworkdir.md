# CreateWorkDir

This script recreates a proper directory tree for packaging.  Its aim is to help
you be organized with the work you do as a packager, and establish a certain
standard for packages' publication, so you don't have to lose much time with
them.  Just package and upload!

It will create a directory tree like this:

    $WORKDIR/
    ├── abslibre/
    │   ├── .git/
    │   ├── libre/<PKGBUILDS>
    │   └── libre-testing/<PKGBUILDS>
    └── staging/
        ├── libre/
        └── libre-testing/

*Related Variables*
  - WORKDIR

## staging/

This directory contains one directory for each repository, where the resulting
packages are in moved for syncing against the main repository on Parabola's
server.  This directory is architecture independent.

## abslibre/

This is the git repo for Parabola's PKGBUILDs.  Here you can find the ABS tree
for our packages, and also where you'll have to put new ones for commit.

(You'll need push access to Parabola's main server, but pulling is public.)

*Related Variables*
  - ABSLIBREGIT
