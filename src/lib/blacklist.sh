#!/usr/bin/env bash
# This may be included with or without `set -euE`

# Copyright (C) 2013-2014, 2016-2018 Luke Shumaker <lukeshu@parabola.nu>
# Copyright (C) 2017 Isaac David <isacdaavid@isacdaavid.info>
#
# License: GNU GPLv2+
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# make sure XDG_CACHE_HOME is set
. "$(librelib conf)"

# Usage: blacklist-normalize <$file
# Normalizes the syntax of the blacklist on stdin.
blacklist-normalize() {
	# dynamically build sed expression based on number of fields
	local -a expr
	local fields=5 sep=: i
	for (( i = $fields - 2; i >= 0; --i )); do
		expr+=('-e' "s/^[^:]*(:[^:]*){${i}}$/&${sep}/")
		sep+=${sep:0:1}
	done
	sed -r -e '/^#/d' "${expr[@]}"
}

# Usage: blacklist-cat
# Prints the blacklist.
# Uses the cache, but downloads it if it doesn't exist.  Also normalizes the blacklist for easier parsing.
blacklist-cat() {
	local file="$XDG_CACHE_HOME/libretools/blacklist.txt"
	if ! [[ -e $file ]]; then
		# exit on failure, whether set -e or not
		blacklist-update || return
	fi
	blacklist-normalize < "$file"
}

# Usage: blacklist-update
# Updates (or creates) the cached copy of the blacklist.
blacklist-update() (
	. "$(librelib messages)"
	load_conf libretools.conf BLACKLIST || return

	local remote_blacklist="$BLACKLIST"
	local local_blacklist="$XDG_CACHE_HOME/libretools/blacklist.txt"

	_l stat_busy "Downloading blacklist of proprietary software packages"

	mkdir -p "${local_blacklist%/*}"

	local tmp_blacklist tmp_log
	tmp_blacklist="$(mktemp "${local_blacklist}.part.XXXXXXXXXX")"
	tmp_log="$(mktemp -t "${0##*/}.wget-log.XXXXXXXXXX")"
	if wget -o "$tmp_log" -O "$tmp_blacklist" -- "$remote_blacklist"; then
		mv -fT -- "$tmp_blacklist" "$local_blacklist"
		rm -f -- "$tmp_log"
		stat_done
	else
		rm -f -- "$tmp_blacklist"
		stat_done
		sed 's/^/    /' <"$tmp_log" >&2
		rm -f -- "$tmp_log"
		if [[ -e "$local_blacklist" ]]; then
			_l warning "Using old local cache of blacklist"
		else
			_l error "Download failed, exiting"
			return 1 # $EXIT_FAILURE
		fi
	fi
)

# Usage: blacklist-cat | blacklist-lookup $pkgname
# Filters to obtain the line for $pkgname from the blacklist on stdin.
# Exits successfully whether a line is found or not.
blacklist-lookup() {
	local pkg=$1
	# we accept that $pkg contains no regex-nes
	blacklist-normalize | grep "^$pkg:" || true
}

# Usage: blacklist-cat | blacklist-get-pkg
# Prints only the package name field of the blacklist line(s) on stdin.
blacklist-get-pkg() {
	blacklist-normalize | cut -d: -f1
}

# Usage: blacklist-cat | blacklist-get-rep
# Prints only the replacement package field of the blacklist line(s) on stdin.
blacklist-get-rep() {
	local -a targets=($(blacklist-get-pkg))
	expac -Ss '%r/%n %n %P %R' | awk -v arr="${targets[*]}" '
	      {
	          gsub("[=<>]+[^[:blank:]]*", "", $0) # discard versioning
	          # build pkg -> providers table from pkg -> provides
	          for (provided = 2; provided <= NF; ++provided) {
	              if (! seen[$1 " " $provided]++) {
	                  providers[$provided] = providers[$provided] $1 " "
	              }
	          }
	      }
	      END {
	          split(arr, targets, " ")
	          for (pkg in targets) {
	              sub("[ \t]+$", "", providers[targets[pkg]])
	              print providers[targets[pkg]]
	          }
	      }'
}

# Usage: blacklist-cat | blacklist-get-url
# Prints URLs formed from the reference-id fields of the blacklist line(s) on stdin.
# Prints an empty line in the absence of reference.
blacklist-get-url() {
	blacklist-normalize | awk -F: '
	    BEGIN {
	        refs["debian"] = "http://bugs.debian.org/"
	        refs["fsf"] = "http://libreplanet.org/wiki/List_of_software_that_does_not_respect_the_Free_System_Distribution_Guidelines#"
	        refs["sv"] = "https://savannah.nongnu.org/bugs/?"
	        refs["fedora"] = "https://bugzilla.redhat.com/show_bug.cgi?id="
	        refs["parabola"] = "https://labs.parabola.nu/issues/"
	    }
	    refs[$3] { print refs[$3] $4 } !refs[$3] { print "" }'
}

# Usage: blacklist-cat | blacklist-get-reason
# Prints only the reason field of the blacklist line(s) on stdin.
blacklist-get-reason() {
	blacklist-normalize | cut -d: -f5-
}
