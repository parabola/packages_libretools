librefetch.conf(5) -- librefetch configuration file
===================================================

## SYNOPSIS

`/etc/libretools.d/librefetch.conf`, `~/.config/libretools/librefetch.conf`

## DESCRIPTION

Configuration for librefetch is stored in `librefetch.conf`.  The
several places it looks for the file are:

 * `/etc/libretools.d/librefetch.conf`
 * `$XDG_CONFIG_HOME/libretools/librefetch.conf`

The later files take precedence over earlier files, but earlier files
are loaded, so that later files only need to set the values they want
to override.

If `$XDG_CONFIG_HOME` is not set, a default value is set:

 * if `$SUDO_USER` is set: `$(eval echo ~$SUDO_USER)/.config`
 * else: `$HOME/.config`

## OPTIONS

 * `MIRRORS=( ... )`:
   A list of locations that generated source tarballs may be located
   at.  If a URL begins with `libre://`, then `libre://` is replaced
   with the first location listed here.
 * `DOWNLOADER='/usr/bin/curl -fLC - --retry 3 --retry-delay 3 -o %o %u'`:
   The HTTP client to use when downloading pre-built source tarballs
   in download mode.  The format and semantics are similar to
   `DLAGENTS` in `makepkg.conf`(5).  If present, `%u` will be replaced
   with the download URL (correctly quoted), otherwise the download
   URL will be appended to the end of the command.  If present, `%o`
   will be replaced with the local filename that it should be
   downloaded to.

## SEE ALSO

librefetch(8), makepkg.conf(5)
