#!/usr/bin/env bash
# lirefetch-install: (un)install librefetch to /etc/makepkg.conf
#
# Copyright (C) 2013-2015, 2017 Luke Shumaker <lukeshu@parabola.nu>
#
# License: GNU GPLv3+
#
# This file is part of Parabola.
#
# Parabola is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Parabola is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Parabola. If not, see <http://www.gnu.org/licenses/>.

set -ueE
. "$(librelib messages)"

# These lines were installed by previous versions of this script
old_code=(
	'# The following line is added by the libretools post_install script'
	'[[ ! -x /usr/bin/librefetch ]] || DLAGENTS+=("libre::/usr/bin/librefetch -p \"\$BUILDFILE\" %u %o")'
	'[[ ! -x /usr/bin/librefetch ]] || DLAGENTS+=({https,libre}"::/usr/bin/librefetch -p \"\$BUILDFILE\" -- %u %o")'
	'DLAGENTS+=({https,libre}"::/usr/bin/librefetch -p $(printf "%q" "$BUILDFILE") -- %u %o")'
	'DLAGENTS+=({https,libre}'\''::/usr/bin/librefetch -p "$BUILDFILE" -- %u %o'\'')'
	'. /etc/libretools.d/librefetch-makepkg.conf # This line was added by librefetch-install'
)

# del_line $file $line
del_line() {
	local file=$1
	local line=$2
	local lineno=($(grep -Fxn -- "$line" "$file" | cut -d: -f1))
	if [[ "${#lineno[@]}" -gt 0 ]]; then
		sed -i "$(printf '%dd;' "${lineno[@]}")" "$file"
	fi
}

# pre_remove $file
pre_remove() {
	local file=$1
	print ":: %s: removing librefetch from %q" "${0##*/}" "$(realpath -s "$file")"

	sed -ri 's/^#(.*) # commented out by the libretools post_install script/\1/' "$file"
	local line
	for line in "${old_code[@]}"; do
		del_line "$file" "$line"
	done
}

usage() {
	print "Usage: %s remove MAKEPKG_CONF_FILE" "${0##*/}"
	print "Removes librefetch to/from makepkg.conf:DLAGENTS"
}

main() {
	if [[ $# != 2 ]]; then
		usage >&2
		return 1
	fi
	local file=$2
	if [[ ! -f "$file" ]]; then
		error "%s: does not exist: %q" "${0##*/}" "$(realpath -s "$file")"
	fi
	if [[ ! -w "$file" ]]; then
		error "%s: cannot write to file: %q" "${0##*/}" "$(realpath -s "$file")"
	fi
	case "$1" in
		remove) pre_remove "$file";;
		*) usage >&2; return 1;;
	esac
}

main "$@"
