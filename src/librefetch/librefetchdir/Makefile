# librefetchdir/Makefile
#   note: This file is part of LibreFetch itself, not part of the
#         build-system.
#
# Copyright (C) 2016 Luke Shumaker <lukeshu@parabola.nu>
#
# License: GNU GPLv3+
#
# This file is part of LibreFetch.
#
# LibreFetch is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LibreFetch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LibreFetch. If not, see <http://www.gnu.org/licenses/>.

# new = 
librefetchdir := $(dir $(lastword $(MAKEFILE_LIST)))

old_makepkg := $(shell which makepkg)
old_library := $(shell $(shell grep LIBRARY= $(old_makepkg)); echo $$LIBRARY)

new_makepkg = $(new)/makepkg
new_library = $(new)/libmakepkg

targets += $(new_makepkg)
targets += $(patsubst $(old_library)/%,$(new_library)/%,$(shell find $(old_library) -type f))
targets += $(new_library)/tidy/~source_date_epoch.sh

all: $(targets)
.PHONY: all

.SECONDARY:
.DELETE_ON_ERROR:

$(new_makepkg): $(old_makepkg) $(librefetchdir)/makepkg.gen
	<$^ | install -Dm755 /dev/stdin $@

$(new_library)/source.sh: \
$(new_library)/%: $(old_library)/% $(librefetchdir)/libmakepkg/%.gen
	<$^ | install -Dm755 /dev/stdin $@

$(new_library)/tidy/purge.sh $(new_library)/tidy/~source_date_epoch.sh: \
$(new_library)/%: $(librefetchdir)/libmakepkg/%
	mkdir -p $(@D)
	ln -sfT $(abspath $< $@)

$(new_library)/%: $(old_library)/%
	mkdir -p $(@D)
	ln -sfT $(abspath $< $@)
