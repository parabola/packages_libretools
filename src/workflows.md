# Workflows

Describe your packaging workflow here!


## fauno's way

During packaging, I don't usually restart a build from scratch if I have to make
changes to the PKGBUILD.  I use a lot of commenting out commands already ran,
`makepkg -R`, etc.  When I used `libremakepkg` I ended up using a lot more
`librechroot` and working from inside the unconfigured chroot, because
`makechrootpkg` (the underlying technology for `libremakepkg`) tries to be too
smart.

When I started writing `treepkg` I found that mounting what I need directly on
the chroot and working from inside it was much more comfortable and simple than
having a makepkg wrapper doing funny stuff (for instance, mangling
`makepkg.conf` and breaking everything.)

This is how the chroot is configured:

* Create the same user (with same uid) on the chroot that the one I use
  regularly.

* Give it password-less sudo on the chroot.

* Bind mount `/home` to `/chroot/home`, where I have the abslibre-mips64el
  clone.

* Bind mount `/var/cache/pacman/pkg` to `/chroot/var/cache/pacman/pkg`

* Put these on system's `fstab` so I don't have to do it everytime

* Configure `makepkg.conf` to `PKGDEST=CacheDir` and `SRCDEST` to something on
  my home.

Workflow:

* Enter the chroot with `systemd-nspawn -D/chroot` and `su - fauno`.

* From another shell (I use tmux) edit the abslibre or search for updates with
  `git log --no-merges --numstat`.

* Pick a package and run `treepkg` from its dir on the chroot, or retake a build
  with `treepkg /tmp/package-treepkg-xxxx`.

 > Note: `treepkg` has been deprecated in favor of `dagpkg`.

What this allows:

* Not having to worry about the state of the chroot.  `chcleanup` removes and
  adds packages in a smart way so shared dependencies stay and others move along
  (think of installing and removing qt for a complete kde rebuild).

* Building many packages in a row without recreating a chroot for every one of
  them.

* Knowing that any change you made to the chroot stays as you want (no one
  touches your makepkg.conf)

* Hability to run regular commands, not through a chroot wrapper.  I can `cd` to
  a dir and use `makepkg -whatever` on it and nothing breaks.

* No extra code spent on wrappers.
