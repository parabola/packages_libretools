Unfortunately, `makechrootpkg.sh` is GPLv2 ONLY.  This means that
everything that uses it must be held to GPLv2+ instead of GPLv3+.  I'm
calling this anything that gets loaded into the same process as it.
