#!/hint/bash -euE -o pipefail
# Copyright (C) 2013, 2017-2018 Luke Shumaker <lukeshu@parabola.nu>
#
# License: GNU GPLv2+
#
# This file is part of Parabola.
#
# Parabola is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# Parabola is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Parabola. If not, see <http://www.gnu.org/licenses/>.

hook_pre_build+=("distcc_start_odaemon")
hook_post_build+=("distcc_stop_odaemon")

_distcc_check() {
	local copydir=$1
	local home=$2

	local files=(
		# From previous versions
		"$home/.makepkg.conf"
		"$copydir/run/distcc-tool.pid"
		"$copydir/run/distcc-tool-idaemon.pid"

		# From this version
		"$copydir/bin/distcc-tool"
		"$copydir/run/distcc-tool-odaemon.pid"
		"$home/.config/pacman/makepkg.conf"
		"$home/.ssh/config"
	)

	local file file_err=false
	for file in "${files[@]}"; do
		if [[ -f $file ]]; then
			file_err=true
			error "Auto-generated file already exists, remove it: %s" "$file"
		fi
	done
	if $file_err; then
		exit 1
	fi
}

distcc_start_odaemon() {
	local copydir=$1

	# Because /{,usr/}{,s}bin are all symlinked together for
	# fileystem 2013.05-2 and up, I can take shortcuts when checking for
	# existance of programs.
	if $NONET && [[ -f "$copydir/bin/socat" && -f "$copydir/bin/distcc" ]]; then
		local home
		if $INCHROOT; then
			home=$LIBREHOME
		else
			home="$copydir/build"
		fi

		_distcc_check "$copydir" "$home"

		local _distcc_tool; _distcc_tool="$(librelib chroot/distcc-tool)"
		install -m755 "$_distcc_tool"  "$copydir/bin/distcc-tool"

		local DISTCC_HOSTS
		DISTCC_HOSTS="$(get_var makepkg DISTCC_HOSTS '')"
		local rewritten_DISTCC_HOSTS
		rewritten_DISTCC_HOSTS=$("$_distcc_tool" rewrite "${DISTCC_HOSTS:-}")

		install -Dm644 /dev/stdin "$home/.config/pacman/makepkg.conf" <<-eot
			trap 'jobs -p | xargs -r kill --' EXIT
			/bin/distcc-tool idaemon ${DISTCC_HOSTS@Q} </dev/null &>/dev/null &
			DISTCC_HOSTS=${rewritten_DISTCC_HOSTS@Q}
			eot

		install -Dm644 /dev/stdin "$home/.ssh/config" <<-'eot'
			Host *
			    ProxyCommand /bin/distcc-tool client %h %p
			eot

		"$_distcc_tool" odaemon "$copydir" </dev/null &>/dev/null &
		echo $! > "$copydir/run/distcc-tool-odaemon.pid"
	fi
}

distcc_stop_odaemon() {
	local copydir=$1

	local home
	if $INCHROOT; then
		home=$LIBREHOME
	else
		home="$copydir/build"
	fi

	if [[ -f "$copydir/run/distcc-tool-odaemon.pid" ]]; then
		local odaemon
		odaemon=$(< "$copydir/run/distcc-tool-odaemon.pid")
		kill -- "$odaemon"

		rm -f -- \
			"$home/.config/pacman/makepkg.conf" \
			"$home/.ssh/config" \
			"$copydir/bin/distcc-tool" \
			"$copydir/run/distcc-tool-odaemon.pid"
	fi
}
