#!/hint/bash -euE -o pipefail
# Copyright (C) 2013, 2017 Luke Shumaker <lukeshu@parabola.nu>
#
# License: GNU GPLv2+
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

hook_check_pkgbuild+=("check_pkgbuild_nonfree")
check_pkgbuild_nonfree() {
	local retval=0
	sudo -EH -u "$LIBREUSER" pkgbuild-check-nonfree -f || retval=$?
	pkgbuild-summarize-nonfree $retval

	if   (( $retval ))
	then warning "Are you making a libre replacement for this package? [y/N]"
	     read -N 1 -s < /dev/tty ; echo ;
	     [[ $REPLY =~ ^[Yy]$ ]] && retval=0
	fi

	! (( $retval ))
}

#hook_check_pkgbuild+=("check_pkgbuild_namcap")
check_pkgbuild_namcap() {
	sudo -EH -u "$LIBREUSER" namcap PKGBUILD
}

#hook_check_pkg+=("check_pkg")
check_pkg() {
	# TODO
	:
}
