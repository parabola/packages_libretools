#!/usr/bin/env bash
# Copyright (C) 2013, 2017 Luke Shumaker <lukeshu@parabola.nu>
#
# License: GNU GPLv3+
#
# This file is part of Parabola.
#
# Parabola is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Parabola is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Parabola.  If not, see <http://www.gnu.org/licenses/>.

. "$(librelib messages)"

# Make sure these match pkgbuild-check-nonfree
declare -ri _E_ERROR=1
declare -ri _E_LIC_UNKNOWN=2
declare -ri _E_LIC_NOGPL=4
declare -ri _E_LIC_NONFREE=8
declare -ri _E_DEP_NONFREE=16
declare -ri _E_PKG_NONFREE=32

usage() {
	print "Usage: %s [OPTIONS] STATUS" "${0##*/}"
	print "Summarizes a status code from pkgbuild-check-nonfree"
	echo
	prose 'It thresholds the issues it finds, only failing for error-level
	       issues, and ignoring warnings.  Unless `-q` is specified, it also
	       prints a summary of the issues it found.'
	echo
	print 'Options:'
	flag '-q' 'Be quiet'
	flag '-h' 'Show this message'
}

main() {
	local quiet=false
	while getopts 'qh' arg; do
		case "$arg" in
			q) quiet=true;;
			h) usage; exit $EXIT_SUCCESS;;
			*) usage >&2; exit $EXIT_INVALIDARGUMENT;;
		esac
	done
	shift $((OPTIND - 1))
	if [[ $# -ne 1 ]]; then
		usage >&2
		exit $EXIT_INVALIDARGUMENT
	fi
	if ! [[ $1 =~ ^[0-9]+$ ]]; then
		error 'STATUS must be an integer'
		usage >&2
		exit $EXIT_INVALIDARGUMENT
	fi

	if $quiet; then
		error() { :; }
		warning() { :; }
	fi

	parse "$1"
}

parse() {
	[[ $# == 1 ]] || panic 'malformed call to parse'
	declare -i s=$1;

	declare -i ret=$EXIT_SUCCESS
	declare -i i
	for i in 1 2 4 8 16 32; do
		if [[ $((s & i)) -gt 0 ]]; then
			case $i in
				$_E_ERROR)
					# could be anything, assume the worst
					error "There was an error processing the PKGBUILD"
					ret=$EXIT_FAILURE;;
				$_E_LIC_UNKNOWN)
					warning "This PKGBUILD has an unknown license";;
				$_E_LIC_NOGPL)
					warning "This PKGBUILD has a GPL-incompatible license";;
				$_E_LIC_NONFREE)
					error "This PKGBUILD has a known nonfree license"
					ret=$EXIT_FAILURE;;
				$_E_DEP_NONFREE)
					error "This PKGBUILD depends on known nonfree packages"
					ret=$EXIT_FAILURE;;
				$_E_PKG_NONFREE)
					error "This PKGBUILD is for a known nonfree package"
					ret=$EXIT_FAILURE;;
			esac
		fi
	done
	return $ret
}

main "$@"
